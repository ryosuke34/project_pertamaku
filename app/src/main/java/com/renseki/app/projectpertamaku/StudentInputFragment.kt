package com.renseki.app.projectpertamaku

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.renseki.app.projectpertamaku.model.Gender
import com.renseki.app.projectpertamaku.model.Mahasiswa
import kotlinx.android.synthetic.main.second_activity.*

class StudentInputFragment : Fragment() {

    private lateinit var listener: StudentInputActionListener

    companion object {
        fun newInstance(listener: StudentInputActionListener): StudentInputFragment {
            val fragment = StudentInputFragment()
            fragment.listener = listener
            return fragment
        }
    }

    interface StudentInputActionListener {
        fun onMahasiswaReady(mahasiswa: Mahasiswa)
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(
                R.layout.student_input_fragment,
                container,
                false
        )

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        btn_add.setOnClickListener {
            val nrp = the_real_nrp.text.toString()
            val name = the_real_full_name.text.toString()
            val isMale = male.isChecked

            sendToOtherFragment(nrp, name, isMale)
        }
    }

    private fun sendToOtherFragment(nrp: String, name: String, male: Boolean) {
        val mhs = Mahasiswa(
                nrp,
                name,
                if (male) Gender.MALE else Gender.FEMALE
        )
        listener.onMahasiswaReady(mhs)
    }
}