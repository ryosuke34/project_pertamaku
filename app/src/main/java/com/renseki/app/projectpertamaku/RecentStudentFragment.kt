package com.renseki.app.projectpertamaku

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.renseki.app.projectpertamaku.model.Gender
import com.renseki.app.projectpertamaku.model.Mahasiswa
import kotlinx.android.synthetic.main.third_activity.*

class RecentStudentFragment : Fragment() {

    companion object {
        fun newInstance() = RecentStudentFragment()
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(
                R.layout.recent_student_fragment,
                container,
                false
        )

    }

    fun receiveStudent(mahasiswa: Mahasiswa) {
        nrp_field.text = mahasiswa.nrp
        name_field.text = mahasiswa.name
        gender_field.text = when(mahasiswa.gender) {
            Gender.MALE -> getString(R.string.male)
            Gender.FEMALE -> getString(R.string.female)
        }
    }
}